## About

This repository is for scripts that help maintain a simple cookbook website.

## License

    Cookbook
    Copyright (C) 2021  Luke Gompertz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3 as
    published by the Free Software Foundation. Later versions of the
    GNU General Public License do not apply to this software.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU General Public License is available along with
    this program. It can also be found at <https://www.gnu.org/licenses/>.
