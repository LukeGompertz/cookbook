import argparse
import sqlite3
import pandas as pd
import os, shutil

def main() -> None:
    argparser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    argparser.add_argument('--database_path', type=str, default='cookbook.db', help='path to database containing cookbook data.', metavar='')
    argparser.add_argument('--output_folder', type=str, default='.', help='output folder', metavar='')
    argparser.add_argument('--recipe_template', type=str, default='templates/recipe_template.html', help='path to recipe template HTML', metavar='')
    argparser.add_argument('--home_template', type=str, default='templates/home_template.html', help='path to main page template HTML', metavar='')
    argparser.add_argument('--tag_template', type=str, default='templates/tag_template.html', help='path to tag template HTML', metavar='')
    args: argparse.Namespace = argparser.parse_args()

    generate_main_page(args = args)
    generate_recipe_pages(args = args)
    generate_tag_pages(args = args)

def generate_main_page(args: argparse.Namespace) -> None:
    with open(file = args.home_template) as f:
        template_text: str = f.read()

    with sqlite3.connect(database = args.database_path) as connection:
        recipe_df: pd.DataFrame = pd.read_sql(sql='''
            SELECT concat('<a href="recipes/', file_name, '.html">', title, '</a>') as html
            FROM recipes
            ORDER BY title''', con=connection)
        
        tag_df: pd.DataFrame = pd.read_sql('''
            SELECT concat('<a href="tags/', file_name, '.html">', tag_name, '</a>') as html
            FROM tags
            ORDER BY tag_name''', con=connection)
        
    template_text = template_text.replace('@RECIPES', recipe_df['html'].str.cat(sep='\n  '))
    template_text = template_text.replace('@TAGS', tag_df['html'].str.cat(sep='\n  '))

    with open(file = args.output_folder + '/cookbook-home.html', mode = 'w') as result_file:
        result_file.write(template_text)

def generate_recipe_pages(args: argparse.Namespace) -> None:

    shutil.rmtree(args.output_folder + '/recipes/', ignore_errors=True) # Delete target folder if it already exists
    os.mkdir(path = args.output_folder + '/recipes/')

    with open(file = args.recipe_template) as f:
        template_text: str = f.read()

    with sqlite3.connect(database = args.database_path) as connection:
        recipe_df: pd.DataFrame = pd.read_sql(sql='''
            SELECT *
            FROM recipes''', con=connection)

    for _, row in recipe_df.iterrows():
        pk_recipes: int = row['pk_recipes']
        text: str = template_text
            
        with sqlite3.connect(database = args.database_path) as connection:
            ingredients_df: pd.DataFrame = pd.read_sql(sql='''
                SELECT
                    case
                        when recipes.file_name is null
                        then ingredient_name
                        else concat('<a href="', recipes.file_name, '.html">', ingredient_name, '</a>')
                    end as 'Ingredient'
                    ,quantity as 'Quantity'
                    ,preparation as 'Preparation'
                    ,alternatives as 'Alternatives'
                    ,note as 'Notes'
                FROM recipe_ingredients
                LEFT JOIN ingredients ON fk_ingredients = pk_ingredients
                LEFT JOIN recipes ON ingredients.fk_recipes = recipes.pk_recipes
                WHERE recipe_ingredients.fk_recipes = @recipe''', con=connection, params={'recipe': pk_recipes})
            ingredients_df.dropna(how='all', axis=1, inplace=True) 
            ingredients_df = ingredients_df.where(pd.notna(ingredients_df), '') #Convert None's to empty strings

            instructions_df: pd.DataFrame = pd.read_sql(sql='''
                SELECT text
                FROM instructions
                WHERE fk_recipes = @recipe
                ORDER BY step''', con=connection, params={'recipe': pk_recipes})

            tags_df: pd.DataFrame = pd.read_sql(sql='''
                SELECT file_name, tag_name, concat('<a class="tag" href="../tags/', file_name, '.html">', tag_name, '</a>') as tag_string
                FROM recipe_tags
                LEFT JOIN tags
                ON fk_tags = pk_tags
                WHERE fk_recipes = @recipe''', con=connection, params={'recipe': pk_recipes})

            usedin_df: pd.DataFrame = pd.read_sql(sql='''
                SELECT file_name, title, concat('<a href="', file_name, '.html">', title, '</a>') as usedin_string
                FROM recipe_ingredients
                LEFT JOIN ingredients
                ON fk_ingredients = pk_ingredients
                LEFT JOIN recipes
                ON recipe_ingredients.fk_recipes = pk_recipes
                WHERE ingredients.fk_recipes = @recipe''', con=connection, params={'recipe': pk_recipes})

        text = text.replace('@TITLE', str(row['title']))
        text = text.replace('@TIMING', str(row['timing']))

        if row['produces'] == None:
            text = text.replace('@PRODUCES', 'N/A')
        else:
            text = text.replace('@PRODUCES', str(row['produces']))

        text = text.replace('@SOURCE', str(row['source']))

        if row['variations'] == None:
            text = text.replace('@VARIATIONS', '<p>None</p>')
        else:
            text = text.replace('@VARIATIONS', str(row['variations']))

        text = text.replace('@INGREDIENTS', ingredients_df.to_html(index=False, classes='ingredients', escape=False))
        text = text.replace('@INSTRUCTIONS', '<li>' + instructions_df['text'].str.cat(sep='</li>\n  <li>') + '</li>')
        text = text.replace('@TAGS', tags_df['tag_string'].str.cat())

        if usedin_df.empty:
            text = text.replace('@USED_IN', 'None')
        else:
            text = text.replace('@USED_IN', usedin_df['usedin_string'].str.cat(sep=', '))

        with open(file = args.output_folder + '/recipes/' + row['file_name'] + '.html', mode = 'w') as result_file:
            result_file.write(text)

def generate_tag_pages(args: argparse.Namespace) -> None:

    shutil.rmtree(args.output_folder + '/tags/', ignore_errors=True) # Delete target folder if it already exists
    os.mkdir(path = args.output_folder + '/tags/')

    with open(file = args.tag_template) as f:
        template_text: str = f.read()

    with sqlite3.connect(database = args.database_path) as connection:
        tag_df: pd.DataFrame = pd.read_sql(sql='''
            SELECT *
            FROM tags''', con=connection)
        
    for _, row in tag_df.iterrows():
        pk_tags: int = row['pk_tags']
        text: str = template_text

        with sqlite3.connect(database = args.database_path) as connection:
            recipe_df: pd.DataFrame = pd.read_sql(sql='''
                SELECT concat('<a href="../recipes/', file_name, '.html">', title, '</a>') as html
                FROM recipe_tags
                LEFT JOIN recipes ON fk_recipes = pk_recipes
                WHERE fk_tags = @tag
                ORDER BY title''', con = connection, params={'tag': pk_tags})
            
        text = text.replace('@TITLE', row['tag_name'])
        text = text.replace('@RECIPES', recipe_df['html'].str.cat(sep='\n  '))

        with open(file = args.output_folder + '/tags/' + row['file_name'] + '.html', mode = 'w') as result_file:
            result_file.write(text)

if __name__ == '__main__':
    main()